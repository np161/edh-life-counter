package my.apps.np161.edhlifetracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Random;

public class TwoPlayer extends Activity {

    Button back, refresh, dice;
    Button playerOneUp, playerOneDown, playerTwoUp, playerTwoDown;
    Button playerOneLife, playerTwoLife;
    Player one, two;
    String tempOne, tempTwo;
    boolean p1Done, p2Done;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_player);

        back = (Button)findViewById(R.id.back);
        refresh = (Button)findViewById(R.id.refresh);
        dice = (Button)findViewById(R.id.dice);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TwoPlayer.this,HomeScreen.class);
                startActivity(i);
                finish();
            }
        });

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(getIntent());
            }
        });

        dice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateFirst();
            }
        });

        one = new Player(20);
        two = new Player(20);

        setupOne();
        setupTwo();


    }

    public void setupOne(){
        playerOneLife = (Button)findViewById(R.id.p1_life);
        playerOneUp = (Button)findViewById(R.id.p1_up);
        playerOneDown = (Button)findViewById(R.id.p1_down);

        tempOne = one.getLife() + "";
        playerOneLife.setText(tempOne);

        playerOneUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setLife(one.getLife() + 1);
                tempOne = one.getLife() + "";
                playerOneLife.setText(tempOne);
            }
        });

        playerOneDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setLife(one.getLife() - 1);
                tempOne = one.getLife() + "";
                playerOneLife.setText(tempOne);
            }
        });

    }

    public void setupTwo(){
        playerTwoLife = (Button)findViewById(R.id.p2_life);
        playerTwoUp = (Button)findViewById(R.id.p2_up);
        playerTwoDown = (Button)findViewById(R.id.p2_down);

        tempTwo = two.getLife() + "";
        playerTwoLife.setText(tempTwo);

        playerTwoUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setLife(two.getLife() + 1);
                tempTwo = two.getLife() + "";
                playerTwoLife.setText(tempTwo);
            }
        });

        playerTwoDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setLife(two.getLife() - 1);
                tempTwo = two.getLife() + "";
                playerTwoLife.setText(tempTwo);
            }
        });
    }

    public void generateFirst(){
        final Random r = new Random();

        Thread t = new Thread(new Runnable() {
            float time = 1.0f;

            int i1;
            int i2;

            @Override
            public void run() {
                Looper.prepare();
                for(int i = 0; i<100;i++) {
                    time += (time/17.5);
                    i1 = r.nextInt(100 - 1) + 1;
                    tempOne = i1 + "";
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                playerOneLife.setText(tempOne);
                            }
                        });

                    i2 = r.nextInt(100 - 1) + 1;
                    tempTwo = i2 + "";
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            playerTwoLife.setText(tempTwo);
                        }
                    });

                    try {
                        Thread.sleep((int)time);
                    }catch(InterruptedException e){

                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        decideWinner(i1, i2);
                    }
                });
            }
        });
        t.start();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putParcelable("Player 1", one);
        savedInstanceState.putParcelable("Player 2", two);
        // ... save more data
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        one = savedInstanceState.getParcelable("Player 1");
        two = savedInstanceState.getParcelable("Player 2");

        setupOne();
        setupTwo();
        // ... recover more data
    }


    public void decideWinner(int one, int two){
        if(one>two){
            Toast.makeText(TwoPlayer.this, "Player One won the roll", Toast.LENGTH_SHORT).show();
        }else if(two>one){
            Toast.makeText(TwoPlayer.this, "Player Two won the roll", Toast.LENGTH_SHORT).show();
        }

        try {
            Thread.sleep(1000);
        }catch(InterruptedException e){
        }

        resetLife();
    }

    public void resetLife(){
        String temp;
        temp = one.getLife() +"";
        playerOneLife.setText(temp);
        temp = two.getLife() + "";
        playerTwoLife.setText(temp);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );
        }
    }
}
