package my.apps.np161.edhlifetracker;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

/**
 * Created by Np161 on 21/02/2016.
 */
public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    private ImageLoader imageLoader;
    private ArrayList<String> list;
    private Resources reso;
    DisplayImageOptions options;

    public ImageAdapter(Context c,Resources res, ArrayList<String> list) {
        mContext = c;
        reso = res;
        this.list = list;
        imageLoader = ImageLoader.getInstance();
        this.imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView.setLayoutParams(new GridView.LayoutParams(conversion(100), conversion(100)));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(20, 20, 20, 20);
            //convertView.setTag(holder);
        } else {
            imageView = (ImageView)convertView;
        }

        imageLoader = ImageLoader.getInstance();
        lazyLoading(imageLoader, ("assets://guilds/" +list.get(position)),imageView, options);
        return imageView;
    }


    private int conversion(int pixels){
        return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pixels, reso.getDisplayMetrics());
    }


    public static void lazyLoading(ImageLoader imageLoader, String tempStr, ImageView imageView,DisplayImageOptions options) {
        imageLoader.displayImage(tempStr, imageView, options,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view,FailReason failReason) {
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view,Bitmap loadedImage) {
                    }
                }, new ImageLoadingProgressListener() {
                    @Override
                    public void onProgressUpdate(String imageUri, View view,int current, int total) {
                    }
                });
    }
}