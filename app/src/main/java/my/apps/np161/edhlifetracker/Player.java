package my.apps.np161.edhlifetracker;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Np161 on 17/02/2016.
 */
public class Player implements Parcelable {

    private int life = 40;
    private int commanderOne = 0;
    private int commanderTwo = 0;
    private int commanderThree = 0;
    private int deaths = 0;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String image = "";


    Player(){
    }

    Player(int life){
        this.life = life;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getCommanderOne() {
        return commanderOne;
    }

    public int getCommanderTwo() {
        return commanderTwo;
    }

    public int getCommanderThree() {
        return commanderThree;
    }

    public void setCommanderOne(int commanderOne) {
        this.commanderOne = commanderOne;
    }

    public void setCommanderTwo(int commanderTwo) {
        this.commanderTwo = commanderTwo;
    }

    public void setCommanderThree(int commanderThree) {
        this.commanderThree = commanderThree;
    }

    public int getLife() {
        return life;
    }

    protected Player(Parcel in) {
        life = in.readInt();
        commanderOne = in.readInt();
        commanderTwo = in.readInt();
        commanderThree = in.readInt();
        deaths = in.readInt();
        image = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(life);
        dest.writeInt(commanderOne);
        dest.writeInt(commanderTwo);
        dest.writeInt(commanderThree);
        dest.writeInt(deaths);
        dest.writeString(image);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Player> CREATOR = new Parcelable.Creator<Player>() {
        @Override
        public Player createFromParcel(Parcel in) {
            return new Player(in);
        }

        @Override
        public Player[] newArray(int size) {
            return new Player[size];
        }
    };
}