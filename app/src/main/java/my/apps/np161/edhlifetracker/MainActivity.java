package my.apps.np161.edhlifetracker;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.OnSheetDismissedListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Player one,two,three,four;
    Button p1plusone, p1minusone, p1plusfive, p1minusfive, p1life, p1cmdpink, p1cmdblue, p1cmdgreen, p1cmdpinkminus, p1cmdblueminus, p1cmdgreenminus ;
    Button p2plusone, p2minusone, p2plusfive, p2minusfive, p2life, p2cmdorange, p2cmdpink, p2cmdblue, p2cmdpinkminus, p2cmdblueminus, p2cmdorangeminus ;
    Button p3plusone, p3minusone, p3plusfive, p3minusfive, p3life, p3cmdorange, p3cmdblue, p3cmdgreen, p3cmdorangeminus, p3cmdblueminus, p3cmdgreenminus ;
    Button p4plusone, p4minusone, p4plusfive, p4minusfive, p4life, p4cmdpink, p4cmdorange, p4cmdgreen, p4cmdpinkminus, p4cmdorangeminus, p4cmdgreenminus ;
    Button p1deathsplus,p2deathsplus,p3deathsplus,p4deathsplus;
    Button p1skull,p2skull,p3skull,p4skull;
    Button back,refresh,dice;
    Switch p1switch, p2switch ,p3switch, p4switch;
    ImageView p1,p2,p3,p4;
    BottomSheetLayout bottomSheet;
    OnSheetDismissedListener dismissedListener;
    String temp;
    String tempOne,tempTwo,tempThree,tempFour;
    String[] imagesww;
    private ImageLoader imageLoader;
    ArrayList<String> listImages;
    DisplayImageOptions options;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageLoader = ImageLoader.getInstance();
        this.imageLoader.init(ImageLoaderConfiguration.createDefault(MainActivity.this));

        try {
            imagesww = getAssets().list("guilds");
            listImages = new ArrayList<String>(Arrays.asList(imagesww));
        } catch (IOException e) {
        }

        bottomSheet = (BottomSheetLayout) findViewById(R.id.bottomsheet);

        back = (Button)findViewById(R.id.back);
        refresh = (Button)findViewById(R.id.refresh);
        dice  = (Button)findViewById(R.id.dice);

        dice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateFirst();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,HomeScreen.class);
                startActivity(i);
                finish();
            }
        });

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(getIntent());
            }
        });

        one = new Player();
        two = new Player();
        three = new Player();
        four = new Player();

        setupOne();
        setupTwo();
        setupThree();
        setupFour();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );
        }
    }

    public void setupOne(){
        p1 = (ImageView)findViewById(R.id.player_one_image);
        if(!one.getImage().equals("")){
            imageLoader = ImageLoader.getInstance();
            lazyLoading(imageLoader, ("assets://guilds/" + one.getImage()), p1, options);
        }else{
            imageLoader = ImageLoader.getInstance();
            lazyLoading(imageLoader, ("assets://uistuff/question.png"), p1, options);
        }
        p1deathsplus = (Button)findViewById(R.id.p1_deaths_plus);
        p1skull = (Button)findViewById(R.id.p1_skull);
        p1life = (Button)findViewById(R.id.p1_life);
        p1plusone = (Button)findViewById(R.id.p1_plus_one);
        p1minusone = (Button)findViewById(R.id.p1_minus_one);
        p1plusfive = (Button)findViewById(R.id.p1_plus_five);
        p1minusfive = (Button)findViewById(R.id.p1_minus_five);
        p1switch = (Switch)findViewById(R.id.p1_switch);
        p1cmdpink = (Button)findViewById(R.id.p1_cmd_pink);
        p1cmdblue = (Button)findViewById(R.id.p1_cmd_blue);
        p1cmdgreen = (Button)findViewById(R.id.p1_cmd_green);
        p1cmdpinkminus = (Button)findViewById(R.id.p1_cmd_pink_minus);
        p1cmdblueminus = (Button)findViewById(R.id.p1_cmd_blue_minus);
        p1cmdgreenminus = (Button)findViewById(R.id.p1_cmd_green_minus);

        p1cmdpink.setVisibility(View.INVISIBLE);
        p1cmdblue.setVisibility(View.INVISIBLE);
        p1cmdgreen.setVisibility(View.INVISIBLE);
        p1cmdpinkminus.setVisibility(View.INVISIBLE);
        p1cmdblueminus.setVisibility(View.INVISIBLE);
        p1cmdgreenminus.setVisibility(View.INVISIBLE);
        p1deathsplus.setVisibility(View.INVISIBLE);
        p1skull.setVisibility(View.INVISIBLE);

        p1switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    p1cmdpink.setVisibility(View.VISIBLE);
                    p1cmdblue.setVisibility(View.VISIBLE);
                    p1cmdgreen.setVisibility(View.VISIBLE);
                    p1cmdpinkminus.setVisibility(View.VISIBLE);
                    p1cmdblueminus.setVisibility(View.VISIBLE);
                    p1cmdgreenminus.setVisibility(View.VISIBLE);
                    p1deathsplus.setVisibility(View.VISIBLE);
                    p1skull.setVisibility(View.VISIBLE);

                    p1life.setVisibility(View.INVISIBLE);
                    p1plusone.setVisibility(View.INVISIBLE);
                    p1minusone.setVisibility(View.INVISIBLE);
                    p1plusfive.setVisibility(View.INVISIBLE);
                    p1minusfive.setVisibility(View.INVISIBLE);
                } else if (!isChecked) {
                    p1cmdpink.setVisibility(View.INVISIBLE);
                    p1cmdblue.setVisibility(View.INVISIBLE);
                    p1cmdgreen.setVisibility(View.INVISIBLE);
                    p1cmdpinkminus.setVisibility(View.INVISIBLE);
                    p1cmdblueminus.setVisibility(View.INVISIBLE);
                    p1cmdgreenminus.setVisibility(View.INVISIBLE);
                    p1deathsplus.setVisibility(View.INVISIBLE);
                    p1skull.setVisibility(View.INVISIBLE);

                    p1life.setVisibility(View.VISIBLE);
                    p1plusone.setVisibility(View.VISIBLE);
                    p1minusone.setVisibility(View.VISIBLE);
                    p1plusfive.setVisibility(View.VISIBLE);
                    p1minusfive.setVisibility(View.VISIBLE);
                }
            }
        });

        temp = one.getLife() + "";
        p1life.setText(temp);
        temp = one.getCommanderOne()+"";
        p1cmdpink.setText(temp);
        temp = one.getCommanderTwo()+"";
        p1cmdblue.setText(temp);
        temp = one.getCommanderThree()+"";
        p1cmdgreen.setText(temp);
        temp = one.getDeaths()+"";
        p1deathsplus.setText(temp);

        p1life.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setLife(one.getLife() - 1);
                temp = one.getLife() + "";
                p1life.setText(temp);
            }
        });

        p1plusone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setLife(one.getLife() + 1);
                temp = one.getLife() + "";
                p1life.setText( temp);
            }
        });

        p1minusone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setLife(one.getLife() - 1);
                temp = one.getLife() + "";
                p1life.setText( temp);
            }
        });

        p1plusfive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setLife(one.getLife()+5);
                temp = one.getLife() + "";
                p1life.setText( temp);
            }
        });

        p1minusfive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setLife(one.getLife() - 5);
                temp = one.getLife() + "";
                p1life.setText( temp);
            }
        });

        p1cmdpink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setCommanderOne(one.getCommanderOne() + 1);
                temp = one.getCommanderOne() + "";
                p1cmdpink.setText(temp);
                one.setLife(one.getLife() - 1);
                temp = one.getLife() + "";
                p1life.setText( temp);
            }
        });

        p1cmdblue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setCommanderTwo(one.getCommanderTwo() + 1);
                temp = one.getCommanderTwo() + "";
                p1cmdblue.setText(temp);
                one.setLife(one.getLife() - 1);
                temp = one.getLife() + "";
                p1life.setText( temp);
            }
        });

        p1cmdgreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setCommanderThree(one.getCommanderThree() + 1);
                temp = one.getCommanderThree() + "";
                p1cmdgreen.setText(temp);
                one.setLife(one.getLife() - 1);
                temp = one.getLife() + "";
                p1life.setText( temp);
            }
        });

        p1cmdpinkminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setCommanderOne(one.getCommanderOne() - 1);
                temp = one.getCommanderOne() + "";
                p1cmdpink.setText(temp);
                one.setLife(one.getLife() + 1);
                temp = one.getLife() + "";
                p1life.setText( temp);
            }
        });

        p1cmdblueminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setCommanderTwo(one.getCommanderTwo() - 1);
                temp = one.getCommanderTwo() + "";
                p1cmdblue.setText(temp);
                one.setLife(one.getLife() + 1);
                temp = one.getLife() + "";
                p1life.setText( temp);
            }
        });

        p1cmdgreenminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setCommanderThree(one.getCommanderThree() - 1);
                temp = one.getCommanderThree() + "";
                p1cmdgreen.setText(temp);
                one.setLife(one.getLife() + 1);
                temp = one.getLife() + "";
                p1life.setText( temp);
            }
        });

        p1deathsplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setDeaths(one.getDeaths() + 1);
                temp = one.getDeaths() + "";
                p1deathsplus.setText(temp);
            }
        });

        p1skull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                one.setDeaths(one.getDeaths() - 1);
                temp = one.getDeaths() + "";
                p1deathsplus.setText(temp);
            }
        });

        p1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheet(p1,one);
            }
        });
    }

    public void setupTwo(){
        p2skull = (Button)findViewById(R.id.p2_skull);
        p2deathsplus = (Button)findViewById(R.id.p2_deaths_plus);
        p2 = (ImageView)findViewById(R.id.player_two_image);
        if(!two.getImage().equals("")){
            imageLoader = ImageLoader.getInstance();
            lazyLoading(imageLoader, ("assets://guilds/" + two.getImage()), p2, options);
        }else{
            imageLoader = ImageLoader.getInstance();
            lazyLoading(imageLoader, ("assets://uistuff/question.png"), p2, options);
        }
        p2life = (Button)findViewById(R.id.p2_life);
        p2plusone = (Button)findViewById(R.id.p2_plus_one);
        p2minusone = (Button)findViewById(R.id.p2_minus_one);
        p2plusfive = (Button)findViewById(R.id.p2_plus_five);
        p2minusfive = (Button)findViewById(R.id.p2_minus_five);
        p2switch = (Switch)findViewById(R.id.p2_switch);
        p2cmdpink = (Button)findViewById(R.id.p2_cmd_pink);
        p2cmdblue = (Button)findViewById(R.id.p2_cmd_blue);
        p2cmdorange = (Button)findViewById(R.id.p2_cmd_orange);
        p2cmdpinkminus = (Button)findViewById(R.id.p2_cmd_pink_minus);
        p2cmdblueminus = (Button)findViewById(R.id.p2_cmd_blue_minus);
        p2cmdorangeminus = (Button)findViewById(R.id.p2_cmd_orange_minus);

        p2cmdpink.setVisibility(View.INVISIBLE);
        p2cmdblue.setVisibility(View.INVISIBLE);
        p2cmdorange.setVisibility(View.INVISIBLE);
        p2cmdpinkminus.setVisibility(View.INVISIBLE);
        p2cmdblueminus.setVisibility(View.INVISIBLE);
        p2cmdorangeminus.setVisibility(View.INVISIBLE);
        p2deathsplus.setVisibility(View.INVISIBLE);
        p2skull.setVisibility(View.INVISIBLE);

        p2switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    p2cmdpink.setVisibility(View.VISIBLE);
                    p2cmdblue.setVisibility(View.VISIBLE);
                    p2cmdorange.setVisibility(View.VISIBLE);
                    p2cmdpinkminus.setVisibility(View.VISIBLE);
                    p2cmdblueminus.setVisibility(View.VISIBLE);
                    p2cmdorangeminus.setVisibility(View.VISIBLE);
                    p2deathsplus.setVisibility(View.VISIBLE);
                    p2skull.setVisibility(View.VISIBLE);

                    p2life.setVisibility(View.INVISIBLE);
                    p2plusone.setVisibility(View.INVISIBLE);
                    p2minusone.setVisibility(View.INVISIBLE);
                    p2plusfive.setVisibility(View.INVISIBLE);
                    p2minusfive.setVisibility(View.INVISIBLE);
                } else if (!isChecked) {
                    p2cmdpink.setVisibility(View.INVISIBLE);
                    p2cmdblue.setVisibility(View.INVISIBLE);
                    p2cmdorange.setVisibility(View.INVISIBLE);
                    p2cmdpinkminus.setVisibility(View.INVISIBLE);
                    p2cmdblueminus.setVisibility(View.INVISIBLE);
                    p2cmdorangeminus.setVisibility(View.INVISIBLE);
                    p2deathsplus.setVisibility(View.INVISIBLE);
                    p2skull.setVisibility(View.INVISIBLE);

                    p2life.setVisibility(View.VISIBLE);
                    p2plusone.setVisibility(View.VISIBLE);
                    p2minusone.setVisibility(View.VISIBLE);
                    p2plusfive.setVisibility(View.VISIBLE);
                    p2minusfive.setVisibility(View.VISIBLE);
                }
            }
        });

        temp = two.getLife() + "";
        p2life.setText(temp);
        temp = two.getCommanderOne()+"";
        p2cmdpink.setText(temp);
        temp = two.getCommanderTwo()+"";
        p2cmdblue.setText(temp);
        temp = two.getCommanderThree()+"";
        p2cmdorange.setText(temp);
        temp = two.getDeaths()+"";
        p2deathsplus.setText(temp);

        p2life.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setLife(two.getLife() - 1);
                temp = two.getLife() + "";
                p2life.setText( temp);
            }
        });

        p2plusone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setLife(two.getLife() + 1);
                temp = two.getLife() + "";
                p2life.setText(temp);
            }
        });

        p2minusone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setLife(two.getLife() - 1);
                temp = two.getLife() + "";
                p2life.setText( temp);
            }
        });

        p2plusfive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setLife(two.getLife()+5);
                temp = two.getLife() + "";
                p2life.setText( temp);
            }
        });

        p2minusfive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setLife(two.getLife() - 5);
                temp = two.getLife() + "";
                p2life.setText( temp);
            }
        });

        //command

        p2cmdpink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setCommanderOne(two.getCommanderOne() + 1);
                temp = two.getCommanderOne() + "";
                p2cmdpink.setText(temp);
                two.setLife(two.getLife() - 1);
                temp = two.getLife() + "";
                p2life.setText( temp);
            }
        });

        p2cmdblue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setCommanderTwo(two.getCommanderTwo() + 1);
                temp = two.getCommanderTwo() + "";
                p2cmdblue.setText(temp);
                two.setLife(two.getLife() - 1);
                temp = two.getLife() + "";
                p2life.setText( temp);
            }
        });

        p2cmdorange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setCommanderThree(two.getCommanderThree() + 1);
                temp = two.getCommanderThree() + "";
                p2cmdorange.setText(temp);
                two.setLife(two.getLife() - 1);
                temp = two.getLife() + "";
                p2life.setText( temp);
            }
        });

        p2cmdpinkminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setCommanderOne(two.getCommanderOne() - 1);
                temp = two.getCommanderOne() + "";
                p2cmdpink.setText(temp);
                two.setLife(two.getLife() + 1);
                temp = two.getLife() + "";
                p2life.setText(temp);
            }
        });

        p2cmdblueminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setCommanderTwo(two.getCommanderTwo() - 1);
                temp = two.getCommanderTwo() + "";
                p2cmdblue.setText(temp);
                two.setLife(two.getLife() + 1);
                temp = two.getLife() + "";
                p2life.setText(temp);
            }
        });

        p2cmdorangeminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setCommanderThree(two.getCommanderThree() - 1);
                temp = two.getCommanderThree() + "";
                p2cmdorange.setText(temp);
                two.setLife(two.getLife() + 1);
                temp = two.getLife() + "";
                p2life.setText(temp);
            }
        });

        p2deathsplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setDeaths(two.getDeaths() + 1);
                temp = two.getDeaths() + "";
                p2deathsplus.setText(temp);
            }
        });

        p2skull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                two.setDeaths(two.getDeaths() - 1);
                temp = two.getDeaths() + "";
                p2deathsplus.setText(temp);
            }
        });

        p2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheet(p2,two);
            }
        });
    }

    public void setupThree(){
        p3skull = (Button)findViewById(R.id.p3_skull);
        p3deathsplus = (Button)findViewById(R.id.p3_deaths_plus);
        p3 = (ImageView)findViewById(R.id.player_three_image);
        if(!three.getImage().equals("")){
            imageLoader = ImageLoader.getInstance();
            lazyLoading(imageLoader, ("assets://guilds/" + three.getImage()), p3, options);
        }else{
            imageLoader = ImageLoader.getInstance();
            lazyLoading(imageLoader, ("assets://uistuff/question.png"), p3, options);
        }
        p3life = (Button)findViewById(R.id.p3_life);
        p3plusone = (Button)findViewById(R.id.p3_plus_one);
        p3minusone = (Button)findViewById(R.id.p3_minus_one);
        p3plusfive = (Button)findViewById(R.id.p3_plus_five);
        p3minusfive = (Button)findViewById(R.id.p3_minus_five);
        p3switch = (Switch)findViewById(R.id.p3_switch);
        p3cmdgreen = (Button)findViewById(R.id.p3_cmd_green);
        p3cmdblue = (Button)findViewById(R.id.p3_cmd_blue);
        p3cmdorange = (Button)findViewById(R.id.p3_cmd_orange);
        p3cmdgreenminus = (Button)findViewById(R.id.p3_cmd_green_minus);
        p3cmdblueminus = (Button)findViewById(R.id.p3_cmd_blue_minus);
        p3cmdorangeminus = (Button)findViewById(R.id.p3_cmd_orange_minus);

        p3cmdorange.setVisibility(View.INVISIBLE);
        p3cmdblue.setVisibility(View.INVISIBLE);
        p3cmdgreen.setVisibility(View.INVISIBLE);
        p3cmdgreenminus.setVisibility(View.INVISIBLE);
        p3cmdblueminus.setVisibility(View.INVISIBLE);
        p3cmdorangeminus.setVisibility(View.INVISIBLE);
        p3deathsplus.setVisibility(View.INVISIBLE);
        p3skull.setVisibility(View.INVISIBLE);


        p3switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    p3cmdgreen.setVisibility(View.VISIBLE);
                    p3cmdblue.setVisibility(View.VISIBLE);
                    p3cmdorange.setVisibility(View.VISIBLE);
                    p3cmdgreenminus.setVisibility(View.VISIBLE);
                    p3cmdblueminus.setVisibility(View.VISIBLE);
                    p3cmdorangeminus.setVisibility(View.VISIBLE);
                    p3deathsplus.setVisibility(View.VISIBLE);
                    p3skull.setVisibility(View.VISIBLE);

                    p3life.setVisibility(View.INVISIBLE);
                    p3plusone.setVisibility(View.INVISIBLE);
                    p3minusone.setVisibility(View.INVISIBLE);
                    p3plusfive.setVisibility(View.INVISIBLE);
                    p3minusfive.setVisibility(View.INVISIBLE);
                } else if (!isChecked) {
                    p3cmdgreen.setVisibility(View.INVISIBLE);
                    p3cmdblue.setVisibility(View.INVISIBLE);
                    p3cmdorange.setVisibility(View.INVISIBLE);
                    p3cmdgreenminus.setVisibility(View.INVISIBLE);
                    p3cmdblueminus.setVisibility(View.INVISIBLE);
                    p3cmdorangeminus.setVisibility(View.INVISIBLE);
                    p3deathsplus.setVisibility(View.INVISIBLE);
                    p3skull.setVisibility(View.INVISIBLE);

                    p3life.setVisibility(View.VISIBLE);
                    p3plusone.setVisibility(View.VISIBLE);
                    p3minusone.setVisibility(View.VISIBLE);
                    p3plusfive.setVisibility(View.VISIBLE);
                    p3minusfive.setVisibility(View.VISIBLE);
                }
            }
        });

        temp = three.getLife() + "";
        p3life.setText(temp);
        temp = three.getCommanderOne()+"";
        p3cmdgreen.setText(temp);
        temp = three.getCommanderTwo()+"";
        p3cmdblue.setText(temp);
        temp = three.getCommanderThree()+"";
        p3cmdorange.setText(temp);
        temp = three.getDeaths()+"";
        p3deathsplus.setText(temp);

        p3life.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                three.setLife(three.getLife() - 1);
                temp = three.getLife() + "";
                p3life.setText(temp);
            }
        });

        p3plusone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                three.setLife(three.getLife() + 1);
                temp = three.getLife() + "";
                p3life.setText(temp);
            }
        });

        p3minusone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                three.setLife(three.getLife() - 1);
                temp = three.getLife() + "";
                p3life.setText(temp);
            }
        });

        p3plusfive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                three.setLife(three.getLife()+5);
                temp = three.getLife() + "";
                p3life.setText(temp);
            }
        });

        p3minusfive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                three.setLife(three.getLife() - 5);
                temp = three.getLife() + "";
                p3life.setText(temp);
            }
        });

        //command

        p3cmdgreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                three.setCommanderOne(three.getCommanderOne() + 1);
                temp = three.getCommanderOne() + "";
                p3cmdgreen.setText(temp);
                three.setLife(three.getLife() - 1);
                temp = three.getLife() + "";
                p3life.setText(temp);
            }
        });

        p3cmdblue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                three.setCommanderTwo(three.getCommanderTwo() + 1);
                temp = three.getCommanderTwo() + "";
                p3cmdblue.setText(temp);
                three.setLife(three.getLife() - 1);
                temp = three.getLife() + "";
                p3life.setText(temp);
            }
        });

        p3cmdorange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                three.setCommanderThree(three.getCommanderThree() + 1);
                temp = three.getCommanderThree() + "";
                p3cmdorange.setText(temp);
                three.setLife(three.getLife() - 1);
                temp = three.getLife() + "";
                p3life.setText(temp);
            }
        });

        p3cmdgreenminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                three.setCommanderOne(three.getCommanderOne() - 1);
                temp = three.getCommanderOne() + "";
                p3cmdgreen.setText(temp);
                three.setLife(three.getLife() + 1);
                temp = three.getLife() + "";
                p3life.setText(temp);
            }
        });

        p3cmdblueminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                three.setCommanderTwo(three.getCommanderTwo() - 1);
                temp = three.getCommanderTwo() + "";
                p3cmdblue.setText(temp);
                three.setLife(three.getLife() + 1);
                temp = three.getLife() + "";
                p3life.setText(temp);
            }
        });

        p3cmdorangeminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                three.setCommanderThree(three.getCommanderThree() - 1);
                temp = three.getCommanderThree() + "";
                p3cmdorange.setText(temp);
                three.setLife(three.getLife() + 1);
                temp = three.getLife() + "";
                p3life.setText(temp);
            }
        });

        p3deathsplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                three.setDeaths(three.getDeaths() + 1);
                temp = three.getDeaths() + "";
                p3deathsplus.setText(temp);
            }
        });

        p3skull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                three.setDeaths(three.getDeaths() - 1);
                temp = three.getDeaths() + "";
                p3deathsplus.setText(temp);
            }
        });

        p3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheet(p3,three);
            }
        });
    }

    public void setupFour(){
        p4skull = (Button)findViewById(R.id.p4_skull);
        p4deathsplus = (Button)findViewById(R.id.p4_deaths_plus);
        p4 = (ImageView)findViewById(R.id.player_four_image);
        if(!four.getImage().equals("")){
            imageLoader = ImageLoader.getInstance();
            lazyLoading(imageLoader, ("assets://guilds/" + four.getImage()), p4, options);
        }else{
            imageLoader = ImageLoader.getInstance();
            lazyLoading(imageLoader, ("assets://uistuff/question.png"), p4, options);
        }
        p4life = (Button)findViewById(R.id.p4_life);
        p4plusone = (Button)findViewById(R.id.p4_plus_one);
        p4minusone = (Button)findViewById(R.id.p4_minus_one);
        p4plusfive = (Button)findViewById(R.id.p4_plus_five);
        p4minusfive = (Button)findViewById(R.id.p4_minus_five);
        p4switch = (Switch)findViewById(R.id.p4_switch);
        p4cmdpink = (Button)findViewById(R.id.p4_cmd_pink);
        p4cmdorange = (Button)findViewById(R.id.p4_cmd_orange);
        p4cmdgreen = (Button)findViewById(R.id.p4_cmd_green);
        p4cmdpinkminus = (Button)findViewById(R.id.p4_cmd_pink_minus);
        p4cmdorangeminus = (Button)findViewById(R.id.p4_cmd_orange_minus);
        p4cmdgreenminus = (Button)findViewById(R.id.p4_cmd_green_minus);

        p4cmdpink.setVisibility(View.INVISIBLE);
        p4cmdorange.setVisibility(View.INVISIBLE);
        p4cmdgreen.setVisibility(View.INVISIBLE);
        p4cmdpinkminus.setVisibility(View.INVISIBLE);
        p4cmdorangeminus.setVisibility(View.INVISIBLE);
        p4cmdgreenminus.setVisibility(View.INVISIBLE);
        p4deathsplus.setVisibility(View.INVISIBLE);
        p4skull.setVisibility(View.INVISIBLE);

        p4switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    p4cmdpink.setVisibility(View.VISIBLE);
                    p4cmdorange.setVisibility(View.VISIBLE);
                    p4cmdgreen.setVisibility(View.VISIBLE);
                    p4cmdpinkminus.setVisibility(View.VISIBLE);
                    p4cmdorangeminus.setVisibility(View.VISIBLE);
                    p4cmdgreenminus.setVisibility(View.VISIBLE);
                    p4deathsplus.setVisibility(View.VISIBLE);
                    p4skull.setVisibility(View.VISIBLE);

                    p4life.setVisibility(View.INVISIBLE);
                    p4plusone.setVisibility(View.INVISIBLE);
                    p4minusone.setVisibility(View.INVISIBLE);
                    p4plusfive.setVisibility(View.INVISIBLE);
                    p4minusfive.setVisibility(View.INVISIBLE);
                } else if (!isChecked) {
                    p4cmdpink.setVisibility(View.INVISIBLE);
                    p4cmdorange.setVisibility(View.INVISIBLE);
                    p4cmdgreen.setVisibility(View.INVISIBLE);
                    p4cmdpinkminus.setVisibility(View.INVISIBLE);
                    p4cmdorangeminus.setVisibility(View.INVISIBLE);
                    p4cmdgreenminus.setVisibility(View.INVISIBLE);
                    p4deathsplus.setVisibility(View.INVISIBLE);
                    p4skull.setVisibility(View.INVISIBLE);

                    p4life.setVisibility(View.VISIBLE);
                    p4plusone.setVisibility(View.VISIBLE);
                    p4minusone.setVisibility(View.VISIBLE);
                    p4plusfive.setVisibility(View.VISIBLE);
                    p4minusfive.setVisibility(View.VISIBLE);
                }
            }
        });

        temp = four.getLife() + "";
        p4life.setText(temp);
        temp = four.getCommanderOne()+"";
        p4cmdpink.setText(temp);
        temp = four.getCommanderTwo()+"";
        p4cmdorange.setText(temp);
        temp = four.getCommanderThree()+"";
        p4cmdgreen.setText(temp);
        temp = four.getDeaths()+"";
        p4deathsplus.setText(temp);

        p4life.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                four.setLife(four.getLife() - 1);
                temp = four.getLife() + "";
                p4life.setText(temp);
            }
        });

        p4plusone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                four.setLife(four.getLife() + 1);
                temp = four.getLife() + "";
                p4life.setText(temp);
            }
        });

        p4minusone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                four.setLife(four.getLife() - 1);
                temp = four.getLife() + "";
                p4life.setText(temp);
            }
        });

        p4plusfive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                four.setLife(four.getLife()+5);
                temp = four.getLife() + "";
                p4life.setText(temp);
            }
        });

        p4minusfive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                four.setLife(four.getLife() - 5);
                temp = four.getLife() + "";
                p4life.setText(temp);
            }
        });

        p4cmdpink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                four.setCommanderOne(four.getCommanderOne() + 1);
                temp = four.getCommanderOne() + "";
                p4cmdpink.setText(temp);
                four.setLife(four.getLife() - 1);
                temp = four.getLife() + "";
                p4life.setText(temp);
            }
        });

        p4cmdorange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                four.setCommanderTwo(four.getCommanderTwo() + 1);
                temp = four.getCommanderTwo() + "";
                p4cmdorange.setText(temp);
                four.setLife(four.getLife() - 1);
                temp = four.getLife() + "";
                p4life.setText(temp);
            }
        });

        p4cmdgreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                four.setCommanderThree(four.getCommanderThree() + 1);
                temp = four.getCommanderThree() + "";
                p4cmdgreen.setText(temp);
                four.setLife(four.getLife() - 1);
                temp = four.getLife() + "";
                p4life.setText(temp);
            }
        });

        p4cmdpinkminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                four.setCommanderOne(four.getCommanderOne() - 1);
                temp = four.getCommanderOne() + "";
                p4cmdpink.setText(temp);
                four.setLife(four.getLife() + 1);
                temp = four.getLife() + "";
                p4life.setText(temp);
            }
        });

        p4cmdorangeminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                four.setCommanderTwo(four.getCommanderTwo() - 1);
                temp = four.getCommanderTwo() + "";
                p4cmdorange.setText(temp);
                four.setLife(four.getLife() + 1);
                temp = four.getLife() + "";
                p4life.setText(temp);
            }
        });

        p4cmdgreenminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                four.setCommanderThree(four.getCommanderThree() - 1);
                temp = four.getCommanderThree() + "";
                p4cmdgreen.setText(temp);
                four.setLife(four.getLife() + 1);
                temp = four.getLife() + "";
                p4life.setText(temp);
            }
        });

        p4deathsplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                four.setDeaths(four.getDeaths() + 1);
                temp = four.getDeaths() + "";
                p4deathsplus.setText(temp);
            }
        });

        p4skull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                four.setDeaths(four.getDeaths() - 1);
                temp = four.getDeaths() + "";
                p4deathsplus.setText(temp);
            }
        });

        p4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheet(p4,four);
            }
        });
    }

    public void generateFirst(){
        final Random r = new Random();

        Thread t = new Thread(new Runnable() {
            float time = 1.0f;
            int i1;
            int i2;
            int i3;
            int i4;

            @Override
            public void run() {
                Looper.prepare();
                for(int i = 0; i<100;i++) {

                    time += (time/17.5);

                    i1 = r.nextInt(100 - 1) + 1;
                    tempOne = i1 + "";
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            p1life.setText(tempOne);
                        }
                    });

                    i2 = r.nextInt(100 - 1) + 1;
                    tempTwo = i2 + "";
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            p2life.setText(tempTwo);
                        }
                    });

                    i3 = r.nextInt(100 - 1) + 1;
                    tempThree = i3 + "";
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            p3life.setText(tempThree);
                        }
                    });

                    i4 = r.nextInt(100 - 1) + 1;
                    tempFour = i4 + "";
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            p4life.setText(tempFour);
                        }
                    });

                    try {
                        Thread.sleep((int)time);
                    }catch(InterruptedException e){
                    }

                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        decideWinner(i1, i2, i3, i4);
                    }
                });
            }
        });
        t.start();
    }

    public void decideWinner(int one, int two, int three, int four){
        if(one>two&&one>three&&one>four){
            Toast.makeText(MainActivity.this, "Player One goes first", Toast.LENGTH_SHORT).show();
        }else if(two>one&&two>three&&two>four){
            Toast.makeText(MainActivity.this, "Player Two goes first", Toast.LENGTH_SHORT).show();
        }else if(three>one&&three>two&&three>four){
            Toast.makeText(MainActivity.this, "Player Three goes first", Toast.LENGTH_SHORT).show();
        }else if(four>one&&four>three&&four>two){
            Toast.makeText(MainActivity.this, "Player Four goes first", Toast.LENGTH_SHORT).show();
        }

        try {
            Thread.sleep(1000);
        }catch(InterruptedException e){
        }

        resetLife();
    }

    public void resetLife(){
        String temp;
        temp = one.getLife() + "";
        p1life.setText(temp);
        temp = two.getLife() + "";
        p2life.setText(temp);
        temp = three.getLife() + "";
        p3life.setText(temp);
        temp = four.getLife() + "";
        p4life.setText(temp);
    }

    public void bottomSheet(final ImageView player, final Player who){
        dismissedListener = new OnSheetDismissedListener() {
            @Override
            public void onDismissed(BottomSheetLayout bottomSheetLayout) {
                bottomSheetLayout.dismissSheet();
            }
        };
        bottomSheet.showWithSheetView(LayoutInflater.from(getApplicationContext()).inflate(R.layout.grid_view, bottomSheet, false));
        bottomSheet.addOnSheetDismissedListener(dismissedListener);

        GridView gridview = (GridView) bottomSheet.findViewById(R.id.gridview);
        ImageAdapter adpater = new ImageAdapter(getApplicationContext(), getResources(),listImages);
        gridview.setAdapter(adpater);


        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                who.setImage(listImages.get(position));
                player.setBackground(null);
                imageLoader = ImageLoader.getInstance();
                lazyLoading(imageLoader, ("assets://guilds/" + listImages.get(position)), player, options);
                bottomSheet.dismissSheet();
            }
        });
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putParcelable("Player 1",one);
        savedInstanceState.putParcelable("Player 2",two);
        savedInstanceState.putParcelable("Player 3", three);
        savedInstanceState.putParcelable("Player 4", four);
        // ... save more data
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        one = savedInstanceState.getParcelable("Player 1");
        two = savedInstanceState.getParcelable("Player 2");
        three = savedInstanceState.getParcelable("Player 3");
        four = savedInstanceState.getParcelable("Player 4");

        setupOne();
        setupTwo();
        setupThree();
        setupFour();
        // ... recover more data
    }

    public static void lazyLoading(ImageLoader imageLoader, String tempStr, ImageView imageView,DisplayImageOptions options) {
        imageLoader.displayImage(tempStr, imageView, options,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view,FailReason failReason) {
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    }
                }, new ImageLoadingProgressListener() {
                    @Override
                    public void onProgressUpdate(String imageUri, View view, int current, int total) {
                    }
                });
    }
}

