package my.apps.np161.edhlifetracker;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;

public class HomeScreen extends Activity {

    Button duel,threePlayers,fourPlayers,fivePlayers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        duel = (Button)findViewById(R.id.duel);
        fourPlayers = (Button)findViewById(R.id.four_player);
        threePlayers = (Button)findViewById(R.id.three_players);

        duel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeScreen.this,TwoPlayer.class);
                startActivity(i);
                finish();
            }
        });

        threePlayers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Intent i = new Intent(HomeScreen.this,Test.class);
                //startActivity(i);
               // finish();
            }
        });

        fourPlayers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeScreen.this,MainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

}
